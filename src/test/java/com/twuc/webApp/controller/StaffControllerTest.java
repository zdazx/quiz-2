package com.twuc.webApp.controller;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.entities.Staff;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class StaffControllerTest extends ApiTestBase {

    @Test
    void should_return_empty_array_when_get_all_staffs_with_staff_table_is_empty() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(content().string("[]"));
    }

    @Test
    void should_create_staff() throws Exception {
        Staff staff = new Staff("AAA", "BBB");
        String staffStr = serialize(staff);
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(staffStr)).andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));
    }

    @Test
    void should_throw_exception_when_create_staff_has_name() throws Exception {
        Staff staff = new Staff();
        String staffStr = serialize(staff);
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(staffStr))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_get_staff_by_id() throws Exception {
        Staff staff = new Staff("AAA", "BBB");
        String staffStr = serialize(staff);
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(staffStr));
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"id\":1,\"firstName\":\"AAA\",\"lastName\":\"BBB\",\"zoneId\":null}"));
    }

    @Test
    void should_set_zone_id_of_staff() throws Exception {
        Staff staff = new Staff("AAA", "BBB");
        String staffStr = serialize(staff);
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(staffStr));

        String zoneId = "Asia/Chongqing";
        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(zoneId))
                .andExpect(status().isOk());

        MvcResult mvcResult = mockMvc.perform(get("/api/staffs/1")).andReturn();
        String contentAsString = mvcResult.getResponse().getContentAsString();
        Staff updatedStaff = mapper.readValue(contentAsString, Staff.class);
        assertEquals(zoneId, updatedStaff.getZoneId());
    }

    @Test
    void should_get_time_zones() throws Exception {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<String> zoneIds = availableZoneIds.stream().sorted().collect(Collectors.toList());

        mockMvc.perform(get("/api/timezones"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(mapper.writeValueAsString(zoneIds)));
    }

    @Test
    void should_create_reservation() throws Exception {
        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "  \"username\": \"Sofia\",\n" +
                        "  \"companyName\": \"ThoughtWorks\",\n" +
                        "  \"zoneId\": \"Africa/Nairobi\",\n" +
                        "  \"startTime\": \"2019-08-20T11:46:00+03:00\",\n" +
                        "  \"duration\": \"PT1H\"\n" +
                        "}"))
        .andExpect(status().isCreated())
        .andExpect(header().string("Location", "/api/staffs/1/reservations"));
    }
}