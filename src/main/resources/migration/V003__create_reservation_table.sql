CREATE TABLE IF NOT EXISTS reservation(
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_name VARCHAR(128) NOT NULL ,
    company_name VARCHAR(64) NOT NULL ,
    zone_id VARCHAR(64) NOT NULL ,
    start_time DATETIME NOT NULL ,
    duration VARCHAR(64) NOT NULL
);