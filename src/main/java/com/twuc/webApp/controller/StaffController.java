package com.twuc.webApp.controller;

import com.twuc.webApp.entities.Reservation;
import com.twuc.webApp.entities.ReservationRequest;
import com.twuc.webApp.entities.Staff;
import com.twuc.webApp.repositories.ReservationRepository;
import com.twuc.webApp.repositories.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api")
public class StaffController {

    @Autowired
    private StaffRepository staffRepo;

    @Autowired
    private ReservationRepository reservationRepo;

    @GetMapping("/staffs")
    public ResponseEntity getAllStaff(){
        List<Staff> staffs = staffRepo.findAll();
        return ResponseEntity.status(HttpStatus.OK)
                .body(staffs);
    }

    @PostMapping("/staffs")
    public ResponseEntity createStaff(@Valid @RequestBody Staff staff){
        Staff savedStaff = staffRepo.save(staff);
        Long staffId = savedStaff.getId();
        staffRepo.flush();
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/" + staffId)
                .build();
    }

    @ExceptionHandler
    public ResponseEntity handleCreateStaff(IllegalArgumentException exception){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @GetMapping("/staffs/{staffId}")
    public ResponseEntity getStaffById(@PathVariable Long staffId){
        Staff staff = staffRepo.findById(staffId).orElseThrow(RuntimeException::new);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(staff);
    }

    @PutMapping("/staffs/{staffId}/timezone")
    public ResponseEntity addZoneIdOfStaff(@PathVariable Long staffId, @RequestBody String zoneId){
        Staff staff = staffRepo.findById(staffId).orElseThrow(RuntimeException::new);
        staff.setZoneId(zoneId);
        staffRepo.save(staff);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/timezones")
    public ResponseEntity getTimeZones(){
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<String> zoneIds = availableZoneIds.stream().sorted().collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(zoneIds);
    }

    @PostMapping("/staffs/{staffId}/reservations")
    public ResponseEntity createReservation(@PathVariable Long staffId,
                                            @RequestBody ReservationRequest reservation){
        Reservation savedReservation = reservationRepo.save(reservation.toReservation());
        Reservation reservation1 = reservationRepo.findById(staffId).get();
        ZonedDateTime zonedDateTime = reservation1.getStartTime().atZone(ZoneId.of("Africa/Nairobi"));
        System.out.println(zonedDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/"+staffId+"/reservations")
                .build();
    }
}
