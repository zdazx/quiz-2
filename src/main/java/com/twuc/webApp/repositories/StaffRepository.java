package com.twuc.webApp.repositories;

import com.twuc.webApp.entities.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface StaffRepository extends JpaRepository<Staff, Long> {

    List<Staff> findAll();

    Optional<Staff> findById(Long id);
}
