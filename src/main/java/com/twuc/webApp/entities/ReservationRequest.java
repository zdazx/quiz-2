package com.twuc.webApp.entities;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

public class ReservationRequest {
    @NotNull
    @Length(max = 128)
    private String username;

    @NotNull
    @Length(max = 64)
    private String companyName;

    @NotNull
    @Length(max = 32)
    private String zoneId;

    @NotNull
    @DateTimeFormat(style = "YYYY-MM-DDThh:mm:ss±hh")
    private ZonedDateTime startTime;

    @NotNull
    @Length(min = 4, max = 4)
    private String duration;

    public ReservationRequest() {
    }

    public ReservationRequest(@NotNull String username, @NotNull String companyName,
                              @NotNull String zoneId, @NotNull ZonedDateTime startTime, @NotNull String duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }

    public Reservation toReservation() {
        return new Reservation(this);
    }
}
