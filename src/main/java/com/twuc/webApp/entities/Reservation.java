package com.twuc.webApp.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;
import java.time.ZonedDateTime;

@Entity
public class Reservation {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 128)
    private String username;

    @Column(nullable = false, length = 64)
    private String companyName;

    @Column(nullable = false)
    private String zoneId;

    @Column(nullable = false)
    private Instant startTime;

    @Column(nullable = false, length = 4)
    private String duration;

    public Reservation() {
    }

    public Reservation(String username, String companyName, String zoneId,
                       ZonedDateTime startTime, String duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime.toInstant();
        this.duration = duration;
    }

    public Reservation(ReservationRequest reservationRequest) {
        this.username = reservationRequest.getUsername();
        this.companyName = reservationRequest.getCompanyName();
        this.duration = reservationRequest.getDuration();
        this.zoneId = reservationRequest.getZoneId();
        this.startTime = reservationRequest.getStartTime().toInstant();
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }
}
